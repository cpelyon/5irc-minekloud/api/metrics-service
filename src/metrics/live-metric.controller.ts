import { Controller, Get, Param, UseGuards } from '@nestjs/common'
import { MetricResponse } from 'src/prometheus/metric'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { PlayersMetricService } from './services/players-metric.service'
import { StatusMetricService } from './services/server-status-metric.service'
import { ResourcesMetricService } from './services/resources-metric.service'
import { ServerPermission } from '../authorization/server-permission.guard'

@ApiTags('live-metrics')
@Controller('live')
@ApiBearerAuth()
@UseGuards(ServerPermission)
export class LiveMetricController {
  constructor(
    private readonly playersMetricService: PlayersMetricService,
    private readonly statusMetricService: StatusMetricService,
    private readonly resourcesMetricService: ResourcesMetricService
  ) {}

  @Get('/servers/:id/players')
  async getPlayerMetric(
    @Param('id') serverId: string
  ): Promise<MetricResponse> {
    console.log(typeof serverId)
    return this.playersMetricService.getLivePlayerMetric(serverId)
  }

  @Get('/servers/:id/status')
  async getStatusMetric(
    @Param('id') serverId: string
  ): Promise<MetricResponse> {
    return this.statusMetricService.getLiveStatusMetric(serverId)
  }

  @Get('/servers/:id/cpu')
  async getCpuMetric(@Param('id') serverId: string): Promise<MetricResponse> {
    return this.resourcesMetricService.getLiveCpuMetric(serverId)
  }

  @Get('/servers/:id/ram')
  async getRamMetric(@Param('id') serverId: string): Promise<MetricResponse> {
    return this.resourcesMetricService.getLiveRamMetric(serverId)
  }
}
