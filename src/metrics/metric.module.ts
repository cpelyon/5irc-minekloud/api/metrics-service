import { Module } from '@nestjs/common'
import { PrometheusModule } from 'src/prometheus/prometheus.module'
import { ServerModule } from 'src/server/server.module'
import { RangeMetricController } from './range-metric.controller'
import { LiveMetricController } from './live-metric.controller'
import { PlayersMetricService } from './services/players-metric.service'
import { StatusMetricService } from './services/server-status-metric.service'
import { ResourcesMetricService } from './services/resources-metric.service'

@Module({
  imports: [ServerModule, PrometheusModule],
  controllers: [LiveMetricController, RangeMetricController],
  providers: [
    PlayersMetricService,
    StatusMetricService,
    ResourcesMetricService,
  ],
  exports: [StatusMetricService],
})
export class MetricModule {}
