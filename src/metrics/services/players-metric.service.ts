import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { MetricQuery } from '../metric-query'
import { MetricResponse } from '../../prometheus/metric'
import { PrometheusService } from '../../prometheus/prometheus.service'

@Injectable()
export class PlayersMetricService {
  private expressionPlayer: string
  private kubernetesMinecraftContainerName: string

  constructor(
    private readonly prometheusService: PrometheusService,
    private readonly configService: ConfigService
  ) {
    this.expressionPlayer = this.configService.get<string>(
      'PROM_EXPRESSION_PLAYER_ONLINE_COUNT'
    )
    this.kubernetesMinecraftContainerName = this.configService.get<string>(
      'K8S_MINECRAFT_CONTAINER_NAME'
    )
  }

  async getLivePlayerMetric(serverId: string): Promise<MetricResponse> {
    return this.prometheusService.getInstantMetric(
      `${this.expressionPlayer}{service="server-${serverId}-${this.kubernetesMinecraftContainerName}"}`
    )
  }

  async getRangePlayerMetric(
    serverId: string,
    query: MetricQuery
  ): Promise<MetricResponse[]> {
    return this.prometheusService.getRangeMetric(
      `${this.expressionPlayer}{service="server-${serverId}-${this.kubernetesMinecraftContainerName}"}`,
      query
    )
  }
}
