import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common'
import { MetricResponse } from 'src/prometheus/metric'
import { MetricQuery } from './metric-query'
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger'
import { PlayersMetricService } from './services/players-metric.service'
import { StatusMetricService } from './services/server-status-metric.service'
import { ResourcesMetricService } from './services/resources-metric.service'
import { ServerPermission } from 'src/authorization/server-permission.guard'

@ApiTags('range-metrics')
@Controller('range')
@ApiBearerAuth()
@UseGuards(ServerPermission)
export class RangeMetricController {
  constructor(
    private readonly playersMetricService: PlayersMetricService,
    private readonly statusMetricService: StatusMetricService,
    private readonly resourcesMetricService: ResourcesMetricService
  ) {}

  @Get('/servers/:id/players')
  async getPlayerMetric(
    @Param('id') serverId: string,
    @Query() query: MetricQuery
  ): Promise<MetricResponse[]> {
    return this.playersMetricService.getRangePlayerMetric(serverId, query)
  }

  @Get('/servers/:id/status')
  async getStatusMetric(
    @Param('id') serverId: string,
    @Query() query: MetricQuery
  ): Promise<MetricResponse[]> {
    return this.statusMetricService.getRangeStatusMetric(serverId, query)
  }

  @Get('/servers/:id/cpu')
  async getCpuMetric(
    @Param('id') serverId: string,
    @Query() query: MetricQuery
  ): Promise<MetricResponse[]> {
    return this.resourcesMetricService.getRangeCpuMetric(serverId, query)
  }

  @Get('/servers/:id/ram')
  async getRamMetric(
    @Param('id') serverId: string,
    @Query() query: MetricQuery
  ): Promise<MetricResponse[]> {
    return this.resourcesMetricService.getRangeRamMetric(serverId, query)
  }
}
