import { ApiProperty } from '@nestjs/swagger'
import { Transform, Type } from 'class-transformer'
import { IsDate, IsEnum, IsNumber, IsOptional } from 'class-validator'

enum Duration {
  second = 's',
  minute = 'm',
  hour = 'h',
  day = 'd',
  week = 'w',
  year = 'y',
}

export class MetricQuery {
  @ApiProperty()
  @IsDate()
  @Transform((value) => new Date(Number(value)), { toClassOnly: true })
  @IsOptional()
  start?: Date

  @ApiProperty()
  @IsDate()
  @Transform((value) => new Date(Number(value)), { toClassOnly: true })
  @IsOptional()
  end?: Date

  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  @IsOptional()
  offset?: number

  @ApiProperty()
  @IsEnum(Duration)
  @IsOptional()
  unit?: Duration
}
