import { Module } from '@nestjs/common'
import { ServerModule } from 'src/server/server.module'
import { ServerPermission } from './server-permission.guard'

@Module({
  imports: [ServerModule],
  providers: [ServerPermission],
  exports: [ServerPermission],
})
export class AuthorizationModule {}
