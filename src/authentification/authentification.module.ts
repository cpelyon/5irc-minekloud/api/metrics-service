import { HttpModule, Module } from '@nestjs/common'
import { AuthService } from './auth.service'

@Module({
  imports: [HttpModule],
  providers: [AuthService],
  exports: [AuthService],
})
export class AuthentificationModule {}
