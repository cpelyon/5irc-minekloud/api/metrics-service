import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketGateway,
  SubscribeMessage,
} from '@nestjs/websockets'
import { Socket } from 'socket.io'

import { UserStatusRequest } from './user-status-request'
import { ServerService } from '../server/server.service'
import { StatusService } from './status.service'
import { AuthService } from 'src/authentification/auth.service'

@WebSocketGateway({ path: '/api/metrics/live/socket.io' })
export class StatusGateway implements OnGatewayConnection, OnGatewayDisconnect {
  private usersConnected: UserStatusRequest[] = []

  constructor(
    private readonly statusService: StatusService,
    private readonly serverService: ServerService,
    private readonly authService: AuthService
  ) {}

  async handleConnection(client: Socket) {
    const handshakeQuery = client.handshake.query

    const jwtValid = await this.authService.isJwtValid(handshakeQuery.token)
    if (!jwtValid) {
      client.disconnect()
      return
    }
    const res = await this.registerUser(
      client,
      handshakeQuery.userId,
      handshakeQuery.serversId.split(',')
    )

    if (!res) {
      client.disconnect()
      return
    }

    console.log(`New client connected with socket ${client.id}`)
  }

  handleDisconnect(client: Socket) {
    this.unregisterUser(client)
    client.disconnect()
  }

  async registerUser(
    client: Socket,
    userId: number,
    serversId: number[]
  ): Promise<boolean> {
    const serversIdAllowed: number[] = []
    await Promise.all(
      serversId.map(async (serverId) => {
        const allowed = await this.serverService.isUserAllowed(userId, serverId)
        if (allowed) {
          console.log(
            `User with id ${userId} is allowed to access server id ${serverId}`
          )
          serversIdAllowed.push(Number(serverId))
        } else {
          console.log(
            `User with id ${userId} is NOT allowed to access server id ${serverId}`
          )
        }
      })
    )

    if (serversIdAllowed.length == 0) {
      return false
    }

    this.usersConnected.push({
      client,
      userId: Number(userId),
      serversId: serversIdAllowed,
    })

    this.statusService.addUserRequestToFetch({
      client,
      userId,
      serversId: serversIdAllowed,
    })

    return true
  }

  async unregisterUser(client: Socket) {
    this.usersConnected = this.usersConnected.filter(
      (user) => user.client.id != client.id
    )

    this.statusService.removeUserRequestToFetch(client)

    console.log(`User with socket id <${client.id}> is now disconnected`)
  }
}
