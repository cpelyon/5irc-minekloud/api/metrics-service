import { ApiProperty } from '@nestjs/swagger'
export class MetricResponse {
  @ApiProperty()
  timestamp: Date
  @ApiProperty()
  value: string
}
