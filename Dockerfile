FROM node:14-alpine AS build-env
WORKDIR /app
COPY package*.json ./
RUN npm ci --prod
COPY ./dist .

FROM gcr.io/distroless/nodejs:14
COPY --from=build-env /app /app
WORKDIR /app
EXPOSE 8080
CMD ["src/main.js"]